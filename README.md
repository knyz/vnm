# VNM
Venom (vnm, virtual nano media) is an experimental
image, audio, and video file format.

It's suppposed to be extremely simple and [suckless](https://suckless.org).
Images are 16 color and run length encoded, It's like 'farbfeld' driven
to its end conclusion. The default color palette is that of TempleOS'.

__Venom is the best set of multimedia formats in the world.__

---

## IMAGES

Magic number letter "V" (86)

uint8 width and height as a single value 
(must be power of two), this is multiplied by 8, max resulting resolution is 
1024x1024
So to get 1024x1024 this value is 128, to get 512x512 you do 64.

row by row uint8 (color) and uint8 (run length)

---

## AUDIO

Audio is always 8000hz
Magic number letter "A" (65)
uint8 list of samples.

---

## VIDEO

Video is at the top, audio at the bottom, Video is 30FPS. 
Width and height is unchanging.

Sequence of "V" images
The audio "A" sequence

---

## TOS COLOR PALETTE

This will be default for all the programs.

00. #000000 (Black) 0,0,0
01. #0000AA (Blue) 0,0,170
02. #00AA00 (Green) 0,170,0
03. #00AAAA (Cyan) 0,170,170
04. #AA0000 (Red) 170,0,0
05. #AA00AA (Magenta) 170,0,170
06. #AA5500 (Brown)  170,79,0
07. #AAAAAA (Light Gray) 170,170,170
08. #555555 (Dark Gray) 79,79,79
09. #5555FF (Light Blue) 79,79,255
10. #55FF55 (Light Green) 79,255,79
11. #55FFFF (Light Cyan) 79,255,255
12. #FF5555 (Light Red) 255,79,79
13. #FF55FF (Light Magenta) 255,79,255
14. #FFFF55 (Yellow) 255,255,79
15. #FFFFFF (White) 255,255,255

---

## Why?

Multimedia sucks. You may notice that all the files are completely
composed of eight bit integers, this allows for multimedia files to
be easily modified by UNIX utilities in interesting interactions, or
be generated very easily. I can see the feasibility of getting a VNM
file to glitch out, but I don't really care, and it has never
happened in practice. Ive discovered that 16 color isnt a such
a hardship, it's quite liberating, especially with proper dithering
images can look indistinguishable and 'cooler'. The palettes of the programs
can also just be modified to best suite a set of images.
VNM is also perfect for embedded assets in ROMs.

---

## PROGRAMS

ff2vnm - Convert farbfeld images to vnm images 


vnmstat - Program that identifies, validates, and makes stats for a vnm file 


vnmnoise - Example program that generates an unoptimized noisy image 


vnmbeat - Example program that generates a sound file 


vnmcarr - Convert VNM files into C arrays 



VNM is so simple, you can just use your core utils to work
with it.



Combining images into a video: cat baby.vnm recursion.vnm > slideshow.vnm


Splitting video into images and audio: csplit -z slideshow.vnm /V/ '{*}'

--- 

## SCRIPTS

png2vnm.sh - Convert PNG to VNM given png2ff


vid2vnm.sh - Convert video to VNM




## PHILANTHROPY

_official VNM donation fund_

Monero (XMR): 48Sxa8J6518gqp4WeGtQ4rLe6SctPrEnnCqm6v6ydjLwRPi9Uh9gvVuUsU2AEDw75meTHCNY8KfU6Txysom4Bn5qPKMJ75w

Wownero (WOW): WW2L2yC6DMg7GArAH3nqXPA6UBoRogf64GodceqA32SeZQpx27xd6rqN82e36KE48a8SAMSoXDB5WawAgVEFKfkw1Q5KSGfX9

If you have philanthropic interest in VNM, contact Vilyaem.

---

## LICENSE


CHRISTIAN FREE SOFTWARE LICENSE
    CFSL

This software is free and open source charity ware, users are asked
to donate to the Eastern Orthodox Church by any means.

Redistribution of this project in source and/or binary forms with/without
modification, are permitted provided that the following conditions are met:

1. Redistributions must retain this notice, the conditions, and the disclaimer.
2. Redistributions must retain credit to the author, and signage to where the original
work can be found.
3. Redistributions cannot become a part of, in anyway shape or form, part of proprietary
software, or software that is clearly out of line with Christian values.
4. Redistributions must remain free, both in price, and what users may do with the software,
the software must remain public and easily accessible.

DISCLAIMER

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
