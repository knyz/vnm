#!/bin/sh
cl
rm bin/*
CC="cc -O3 -std=c89 -Wall -Wpedantic -Wextra -Werror "
$CC programs/ff2vnm.c -o bin/ff2vnm
$CC programs/vnmbeat.c -o bin/vnmbeat
$CC programs/vnmcarr.c -o bin/vnmcarr
$CC programs/vnmnoise.c -o bin/vnmnoise
$CC programs/vnmstat.c -o bin/vnmstat
doas cp bin/* /usr/bin/
