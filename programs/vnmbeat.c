/*********************************************
 * Description - VNMBeat, example program
 * that generates a sound in VNM
 *
 * ./vnmbeat > vnmbeat.vnm
 *
 * Author - Vilyaem
 * Date - Apr 28 2024
 * *******************************************/

#include <stdio.h>

int main(void){
  int i;
  putchar('A');
  for ( i = 0; i < 1000000; i++)
    putchar(
        ( (i * i ^ i) | ((i % 73) * i*8 & i >> 24))  /*play with this to make music*/
        );
  return 0;
}
