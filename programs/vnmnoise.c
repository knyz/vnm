/*********************************************
 * Description - Example VNM noise generator
 * THis does no attempt to make the output tiny,
 * this is a demonstration on how easy it is to produce
 * vnm files.
 *
 * ./vnmnoise > noise.vnm
 *
 * Author - Vilyaem
 * Date - Apr 26 2024
 * *******************************************/

#include <stdio.h>
#include <stdlib.h>

#define RES 1024 /*Our resolution*/

int main(void){
  int i;
  putchar('V');
  putchar(RES/8);
  for(i = 0; i != RES*RES;i++){
    putchar(1);
    putchar(i%rand());
  }
  return 0;
}
