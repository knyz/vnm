/*********************************************
 * Description - VNM Stat, report and identify on VNM
 * files
 *
 * < file.vnm | ./vnmstat
 *
 * Author - Vilyaem
 * Date - Apr 26 2024
 * *******************************************/


/*----------PREPROCESSOR----------*/

#include <stdio.h>
#include <stdlib.h>

#define IMG 0
#define AUD 1
#define VID 2

/*----------GLOBALS----------*/
int c;
int charcnt = 0;
int framecnt = 0;
int mode = -1;
int res = 0;

/*----------FUNCTIONS----------*/

/*********************************************
 * Description - Main
 * Author - Vilyaem
 * Date - Apr 26 2024
 * *******************************************/
int main(void){

  /*Recieve file via STDIN*/
  while(c != EOF){
    c = fgetc(stdin);

    /*Determine the type of file*/
    if(charcnt == 0 && c == 'V'){
      mode = IMG;
    }
    else if(charcnt == 0 && c == 'A'){
      mode = AUD;
    }
    /*Determine if the image is actually a video*/
    if((mode == IMG && charcnt != 0) && (c == 'V' || c == 'A')){
      mode = VID;
      framecnt = 1; /*(counting the first one)*/
    }
    /*Determine resolution*/
    if(mode == IMG && charcnt == 1){
      res = c * 8;
    }
    /*Count up frames*/
    if(mode == VID && c == 'V'){
      framecnt++;
    }


    charcnt++;
  }

  /*Print the results*/
  puts("---VNM STATS---");
  switch(mode){
    case IMG: printf("This is an image, it has %d bytes of data, the resolution is %d.\n",charcnt,res); break;
    case AUD: printf("This is an audio file, it has %d bytes of data.\n",charcnt); break;
    case VID: printf("This is a video, it has %d bytes of data, it has %d frames, the resolution is %d.\n",charcnt,framecnt,res); break;
    default:puts("error: bad file");break;
  }
  return 0;
}
