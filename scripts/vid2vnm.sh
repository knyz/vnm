#!/bin/sh
clear
input_video="$1"
rm result.vnm
## Convert video to PNG frames at 30 FPS
ffmpeg -r 30 -i "$input_video" frame-%04d.png
ls
## Process the frames into farbfeld format with ff2png
find -type f . -name "*.png" -exec cat {} | ff2png >> {}.ff \;
ls
## Process the farbfeld images to VNM images
find -type f . -name "*.png.ff" -exec ff2vnm {} {}.vnm \;
ls
## Extract audio from video to an 8000hz WAV file
ffmpeg -ar 8000 -ac 1 -i "$input_video" AUDIO.raw
cat *.vnm > result.vnm
echo "A" >> result.vnm
cat AUDIO.wav >> result.vnm
rm frame*
rm AUDIO.wav
